all: private-tmpdir.so

private-tmpdir.so: private-tmpdir.c
	gcc -I${SLURMDIR} -std=gnu99 -Wall -o private-tmpdir.o -fPIC -c private-tmpdir.c
	gcc -shared -o private-tmpdir.so private-tmpdir.o

install: private-tmpdir.so
	install -m 0755 private-tmpdir.so ${DESTDIR}${PREFIX}/lib/slurm

clean:
	rm -f private-tmpdir.o private-tmpdir.so

